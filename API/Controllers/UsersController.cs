using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data;
using API.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
   
    public class UsersController : BaseApiController
    {
        private readonly DataContext _context; // using the _context will let us manipulate the
        // database

        // Establishes a connection with the database
        public UsersController(DataContext context)
        {
            _context = context;
        }

        /* Endpoints */ 

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<AppUser>>> GetUsers(){
            return await _context.Users.ToListAsync();  // returns a list of users (IEnumerable actually)
        }

        // api/users/3    <-- that is an example of the url the user sees
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<AppUser>> GetUser(int id){
            return await _context.Users.FindAsync(id); 
        }
    }
}