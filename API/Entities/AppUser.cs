namespace API.Entities
{
    public class AppUser
    {
        public int Id { get; set; }// This Id is a convention. It is recognized by Entity Framework as an ID

        public string UserName { get; set; } 

        public byte[] PasswordHash { get; set; }

        public byte[] PasswordSalt { get; set; }
    }
}