using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using API.Entities;
using API.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace API.Services
{
    public class TokenService : ITokenService
    {
        // The Symmetric key is used to both encrypt and decrypt our coded info
        private readonly SymmetricSecurityKey _key; 

        //Constructor needed for the injection of our configuration into this class
        public TokenService(IConfiguration config){
            // The key is created by hashing some other string, just like always. But this string
            // will live at the config file. 
            _key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["TokenKey"]));
        }
        public string CreateToken(AppUser user)
        {
            // Here we are specifying the things that the token will be claiming to be
            // The claim type is a NameId and the value is held by the user.UserName
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.NameId, user.UserName)
            };

            // Here we have the Credentials. Our _key will be signed with the HmacSha512Signature
            var creds = new SigningCredentials(_key, SecurityAlgorithms.HmacSha512Signature); 

            // Here we specify what will be inside our token
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(7),
                SigningCredentials = creds
            }; 

            var tokenHandler = new JwtSecurityTokenHandler(); // We have to do this to create token

            var token = tokenHandler.CreateToken(tokenDescriptor); // The CreateToken receives what token will have

            return tokenHandler.WriteToken(token); // WriteToken returns a jwt with 'token' characteristics
        }
    }
}